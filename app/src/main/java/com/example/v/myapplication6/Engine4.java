package com.example.v.myapplication6;


import android.annotation.SuppressLint;
import android.content.*;
import android.net.wifi.*;
import android.net.wifi.p2p.WifiP2pManager;
import android.os.*;
import android.support.v7.widget.Toolbar;
import android.text.method.ScrollingMovementMethod;
import android.view.*;
import android.view.View.*;
import android.widget.*;

import com.example.v.myapplication6.util.*;

import com.example.v.myapplication6.construct.*;
import com.example.v.myapplication6.net.wifi.*;

import java.util.*;


public class Engine4 extends AbstractEngine implements OnClickListener {
    private Handler handler = new Handler();


    public Knight k1;
    public Knight k2;

    public boolean initialize = true;
    public Timer redrawTimer;

    //events timer marks
    public long firstSecondTimeMark = System.currentTimeMillis();
    public long firstCustomPeriodTimeMark = firstSecondTimeMark;
    public long firstHalfSecondTimeMark = firstSecondTimeMark;
    public long firstTwoSecondsTimeMark = firstSecondTimeMark;
    public long currentTimeMark;
    public boolean oneSecondPassed;
    public boolean customPeriodTimePassed;
    public boolean oneHalfSecondPassed;
    public boolean twoSecondsPassed;
    public WifiManager wifiManager;
    public WifiReceiver receiverWifi;
    public WifiP2pManager manager;
    public WifiP2pManager.Channel channel;

    WifiP2pManager mManager;
    WifiP2pManager.Channel mChannel;
    WiFiDirectBroadcastReceiver mReceiver;
    public WiFiDirectBroadcastReceiver wiFiDirectBroadcastReceiver;
    public boolean wifiPeersFoundFlag;

    String dbgText;

    public void calcTimePassed() {
        //logger.trace("---");
        currentTimeMark = System.currentTimeMillis();
        //logger.info("---");



        if((currentTimeMark - firstCustomPeriodTimeMark) > 30) {
            //logger.info("oneSecondPassed");
            customPeriodTimePassed = true;
            firstCustomPeriodTimeMark = currentTimeMark;
            drawImageSequenceTask();
        }

        if((currentTimeMark - firstSecondTimeMark) > 1000) {
            //logger.info("oneSecondPassed");
            oneSecondPassed = true;
            firstSecondTimeMark = currentTimeMark;
        }
        if((currentTimeMark - firstHalfSecondTimeMark) > 500) {
            //logger.info("oneHalfSecondPassed");
            oneHalfSecondPassed = true;
            firstHalfSecondTimeMark = currentTimeMark;
        }
        if((currentTimeMark - firstTwoSecondsTimeMark) > 2000) {
            //logger.info("oneHalfSecondPassed");
            twoSecondsPassed = true;
            firstTwoSecondsTimeMark = currentTimeMark;

            //k2.staminaBar.extendedDebugPrint();
            //k2.weapon.extendedDebugPrint();
            //k2.body.extendedDebugPrint();

        }
    }

    public void resetEventsTimer() {
        //logger.trace("---");
        firstSecondTimeMark = System.currentTimeMillis();
        firstHalfSecondTimeMark = firstSecondTimeMark;
        firstTwoSecondsTimeMark = firstSecondTimeMark;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main4);
        init();
    }

    public void init() {
        //frameWidth = 350;
        //frameHeight = 250;
        frameWidth = 800;
        frameHeight = 400;

        initWiFi();
        initHandler();
        initKnights();
        initButtons(); //buttons must be initiated after knights because they use some knights initial values


        //TODO - init dbg
        ((TextView)findViewById(R.id.debug_text)).setMovementMethod(new ScrollingMovementMethod());


        //((Toolbar)findViewById(R.id.toolbar2)).setVisibility(View.INVISIBLE);

    }

    public void reset() {
        //logger.trace("---");

        initKnights();

        ((Toolbar)findViewById(R.id.toolbar)).setVisibility(View.VISIBLE);
        ((Toolbar)findViewById(R.id.toolbar2)).setVisibility(View.VISIBLE);
    }

    public void initHandler() {
        Handler h = new Handler();
        //We can't initialize the graphics immediately because the layout manager
        //needs to run first, thus call back in a sec.
        h.postDelayed(new Runnable() {
            @Override
            public void run() {
                initGfx();
            }
        }, 1000);
    }

    synchronized public void initGfx() {
        handler.removeCallbacks(frameUpdate);
        ((DrawField)findViewById(R.id.the_canvas)).invalidate();

        stepCadre();

        handler.postDelayed(frameUpdate, Const.FRAME_RATE);
    }

    private Runnable frameUpdate = new Runnable() {
        @Override
        synchronized public void run() {
            initGfx();
        }
    };

    @SuppressLint("WifiManagerLeak")
    public void initWiFi() {
        wifiManager = (WifiManager) getSystemService(Context.WIFI_SERVICE);
        wifiManager.setWifiEnabled(true);

        receiverWifi = new WifiReceiver();
        registerReceiver(receiverWifi, new IntentFilter(WifiManager.SCAN_RESULTS_AVAILABLE_ACTION));
        receiverWifi.setEngine(this);

        wifiManager.startScan();

        //ListView lvWifiDetails;
        //List wifiList;
        wifiManager.startScan();
        List<ScanResult> wifiList = wifiManager.getScanResults();

        //wifiManager.get

        //customListAdapter = new CustomListAdapter(wifiList);

        //dbgText = customListAdapter.getResult(wifiList);
        dbgText = getResult(wifiList);



        mManager = (WifiP2pManager) getSystemService(Context.WIFI_P2P_SERVICE);
        //TODO - mb crashes on channel because 2nd param sets looper which can be destroyed on man redraw method handler.removeCallbacks
        mChannel = mManager.initialize(this, getMainLooper(), null);
        mReceiver = new WiFiDirectBroadcastReceiver(mManager, mChannel, this);


        /*
        mManager.discoverPeers(channel, new WifiP2pManager.ActionListener() {
            @Override
            public void onSuccess() {
                wifiPeersFoundFlag = true;
            }

            @Override
            public void onFailure(int reasonCode) {
                wifiPeersFoundFlag = false;
            }
        });
        */
        dbgText = "mReceiver.wifiP2pEnabledFlag=" + mReceiver.wifiP2pEnabledFlag
                    + "\nwifiPeersFoundFlag=" + wifiPeersFoundFlag
        ;

    }

    //TODO - print dbg
    public void printDbgText(String dbgText) {
        ((TextView)findViewById(R.id.debug_text)).setText(
                "\n" + dbgText + "\n"
        );
    }

    public String getResult(List<ScanResult> wifiList) {
        String result = "";
        int counter = 0;

        for(ScanResult scanResult : wifiList) {
            ++counter;
            result += "SSID :: " + scanResult.SSID;
            result +=  "\nStrength :: " + scanResult.level;
            result +=  "\nBSSID :: " + scanResult.BSSID;
            result +=  "\nChannel :: ";
            result +=  convertFrequencyToChannel(scanResult.frequency);
            result +=  "\nFrequency :: " + scanResult.frequency;
            result +=  "\nCapability :: " + scanResult.capabilities;
        }
        result += "\ncounter=" + counter;

        return result;
    }

    public static int convertFrequencyToChannel(int freq) {
        if (freq>= 2412 &&freq<= 2484) { return (freq - 2412) / 5 + 1; } else if (freq>= 5170 && freq<= 5825) {
            return (freq - 5170) / 5 + 34;
        } else {
            return -1;
        }
    }

    public void handleAttackStart(Knight k) {
        k.drawAttackAttemptFlag = true;
        k.calcStaminaLoss(this);
        //k.attackMomentFinishedFlag = false;
    }

    public void handleAttackForwardStart(Knight k) {
        k.drawAttackForwardAttemptFlag = true;
        handleAttackStart(k);
        k.calcStaminaLoss(this);
    }

    public void handleAttackStop(Knight k) {
        k.drawAttackAttemptFlag = false;
        k.drawAttackAttemptAllowFlag = false;
        k.damageDone = false;
        k.imageTask.setImageId(k.imageStandId);
        k.staminaUsed = false;
        k.imageSequenceTaskCurrentIndex = 0;
    }

    public void handleAttackForwardStop(Knight k) {
        k.drawAttackForwardAttemptFlag = false;
        calcAttack();
        k.damageDone = false;
        k.staminaForwardAttackUsed = false;
    }

    public void initButtons() {
        ((Button)findViewById(R.id.hide_panel1_button)).setOnClickListener(this);
        ((Button)findViewById(R.id.hide_panel2_button)).setOnClickListener(this);
        ((Button)findViewById(R.id.reset_button)).setOnClickListener(this);
        ((Button)findViewById(R.id.reset_button2)).setOnClickListener(this);
        ((Button)findViewById(R.id.button_left)).setOnClickListener(this);
        ((Button)findViewById(R.id.button_left2)).setOnClickListener(this);
        ((Button)findViewById(R.id.button_right)).setOnClickListener(this);
        ((Button)findViewById(R.id.button_right2)).setOnClickListener(this);
        ((Button)findViewById(R.id.button_low)).setOnClickListener(this);
        ((Button)findViewById(R.id.button_low2)).setOnClickListener(this);

        ((Button)findViewById(R.id.reset_button)).setEnabled(true);
        ((Button)findViewById(R.id.reset_button2)).setEnabled(true);
        ((Button)findViewById(R.id.hide_panel1_button)).setEnabled(true);
        ((Button)findViewById(R.id.hide_panel2_button)).setEnabled(true);
        ((Button)findViewById(R.id.button_left)).setEnabled(true);
        ((Button)findViewById(R.id.button_left2)).setEnabled(true);
        ((Button)findViewById(R.id.button_right)).setEnabled(true);
        ((Button)findViewById(R.id.button_right2)).setEnabled(true);
        ((Button)findViewById(R.id.button_low)).setEnabled(true);
        ((Button)findViewById(R.id.button_low2)).setEnabled(true);

        ((Button)findViewById(R.id.button_right)).setOnClickListener(this);
        ((Button)findViewById(R.id.button_right)).setEnabled(true);
        ((Button)findViewById(R.id.button_right)).setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    k1.moveForwardFlag = true;
                    k1.xSpeed = Const.X_SPEED_VALUE;
                    if(k1.drawAttackAttemptFlag) {
                        handleAttackForwardStart(k1);
                    }
                } else if (event.getAction() == MotionEvent.ACTION_UP) {
                    k1.moveForwardFlag = false;
                    k1.xSpeed = 0;
                    if(k1.drawAttackAttemptFlag) {
                        handleAttackForwardStop(k1);
                    }
                }
                return true;
            }
        });
        ((Button)findViewById(R.id.button_right2)).setOnClickListener(this);
        ((Button)findViewById(R.id.button_right2)).setEnabled(true);
        ((Button)findViewById(R.id.button_right2)).setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    k2.moveForwardFlag = true;
                    k2.xSpeed = Const.X_SPEED_VALUE;
                    if(k2.drawAttackAttemptFlag) {
                        handleAttackForwardStart(k2);
                    }
                } else if (event.getAction() == MotionEvent.ACTION_UP) {
                    k2.moveForwardFlag = false;
                    k2.xSpeed = 0;
                    if(k2.drawAttackAttemptFlag) {
                        handleAttackForwardStop(k2);
                    }
                }
                return true;
            }
        });
        ((Button)findViewById(R.id.button_left)).setOnClickListener(this);
        ((Button)findViewById(R.id.button_left)).setEnabled(true);
        ((Button)findViewById(R.id.button_left)).setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    k1.xSpeed = - Const.X_SPEED_VALUE;
                    resetCollisionFlags();
                } else if (event.getAction() == MotionEvent.ACTION_UP) {
                    k1.xSpeed = 0;
                }
                return true;
            }
        });
        ((Button)findViewById(R.id.button_left2)).setOnClickListener(this);
        ((Button)findViewById(R.id.button_left2)).setEnabled(true);
        ((Button)findViewById(R.id.button_left2)).setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    k2.xSpeed = - Const.X_SPEED_VALUE;
                    resetCollisionFlags();
                } else if (event.getAction() == MotionEvent.ACTION_UP) {
                    k2.xSpeed = 0;
                }
                return true;
            }
        });

        ((Button)findViewById(R.id.button_low)).setOnClickListener(this);
        ((Button)findViewById(R.id.button_low)).setEnabled(true);
        ((Button)findViewById(R.id.button_low)).setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    k1.lowFlag = true;
                } else if (event.getAction() == MotionEvent.ACTION_UP) {
                    k1.lowFlag = false;
                }


                //String result = "";
                //TODO - print dbg
                //initWiFi();
                printDbgText("k1.lowFlag=" + k1.lowFlag + "\n" +
                        "dbgText=" + dbgText);
                return true;
            }
        });
        ((Button)findViewById(R.id.button_low2)).setOnClickListener(this);
        ((Button)findViewById(R.id.button_low2)).setEnabled(true);
        ((Button)findViewById(R.id.button_low2)).setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    k2.lowFlag = true;
                } else if (event.getAction() == MotionEvent.ACTION_UP) {
                    k2.lowFlag = false;
                }
                //TODO - print dbg
                printDbgText("k2.lowFlag=" + k2.lowFlag);
                return true;
            }
        });



        ((Button)findViewById(R.id.button_attack)).setOnClickListener(this);
        ((Button)findViewById(R.id.button_attack)).setEnabled(true);
        ((Button)findViewById(R.id.button_attack)).setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    //TODO - print dbg
                    printDbgText("k1.lowFlag=" + k1.lowFlag +
                            "attack" +
                            System.currentTimeMillis() +
                            k1.staminaBar.getDebugPrint() +
                            "imagesInitCounter=" + ((DrawField)findViewById(R.id.the_canvas)).imagesInitCounter);

                    handleAttackStart(k1);
                } else if (event.getAction() == MotionEvent.ACTION_UP) {
                    handleAttackStop(k1);
                }
                return true;
            }
        });
        ((Button)findViewById(R.id.button_attack2)).setOnClickListener(this);
        ((Button)findViewById(R.id.button_attack2)).setEnabled(true);
        ((Button)findViewById(R.id.button_attack2)).setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    //TODO - print dbg
                    printDbgText("k2.lowFlag=" + k2.lowFlag +
                            "attack" +
                            System.currentTimeMillis() +
                            k2.staminaBar.getDebugPrint() +
                            "imagesInitCounter=" + ((DrawField)findViewById(R.id.the_canvas)).imagesInitCounter);

                    handleAttackStart(k2);
                } else if (event.getAction() == MotionEvent.ACTION_UP) {
                    handleAttackStop(k2);
                }
                return true;
            }
        });

        ((Button)findViewById(R.id.button_shield)).setOnClickListener(this);
        ((Button)findViewById(R.id.button_shield)).setEnabled(true);
        ((Button)findViewById(R.id.button_shield)).setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    if(!k1.lowFlag) {
                        k1.drawShieldUp = true;
                    } else {
                        k1.drawShieldLow = true;
                    }
                } else if (event.getAction() == MotionEvent.ACTION_UP) {
                    k1.drawShieldUp = false;
                    k1.drawShieldLow = false;
                    k1.imageTask.setImageId(k1.imageStandId);
                }
                return true;
            }
        });
        ((Button)findViewById(R.id.button_shield2)).setOnClickListener(this);
        ((Button)findViewById(R.id.button_shield2)).setEnabled(true);
        ((Button)findViewById(R.id.button_shield2)).setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    if(!k2.lowFlag) {
                        k2.drawShieldUp = true;
                    } else {
                        k2.drawShieldLow = true;
                    }
                } else if (event.getAction() == MotionEvent.ACTION_UP) {
                    k2.drawShieldUp = false;
                    k2.drawShieldLow = false;
                    k2.imageTask.setImageId(k2.imageStandId);
                }
                return true;
            }
        });

        ((Button)findViewById(R.id.reset_button)).setOnClickListener(this);
        ((Button)findViewById(R.id.reset_button)).setEnabled(true);
        ((Button)findViewById(R.id.reset_button)).setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                } else if (event.getAction() == MotionEvent.ACTION_UP) {
                    reset();
                }
                return true;
            }
        });
        ((Button)findViewById(R.id.reset_button2)).setOnClickListener(this);
        ((Button)findViewById(R.id.reset_button2)).setEnabled(true);
        ((Button)findViewById(R.id.reset_button2)).setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                } else if (event.getAction() == MotionEvent.ACTION_UP) {
                    reset();
                }
                return true;
            }
        });

        ((Button)findViewById(R.id.hide_panel1_button)).setOnClickListener(this);
        ((Button)findViewById(R.id.hide_panel1_button)).setEnabled(true);
        ((Button)findViewById(R.id.hide_panel1_button)).setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                } else if (event.getAction() == MotionEvent.ACTION_UP) {
                    ((Toolbar)findViewById(R.id.toolbar)).setVisibility(View.INVISIBLE);
                }
                return true;
            }
        });
        ((Button)findViewById(R.id.hide_panel2_button)).setOnClickListener(this);
        ((Button)findViewById(R.id.hide_panel2_button)).setEnabled(true);
        ((Button)findViewById(R.id.hide_panel2_button)).setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                } else if (event.getAction() == MotionEvent.ACTION_UP) {
                    ((Toolbar)findViewById(R.id.toolbar2)).setVisibility(View.INVISIBLE);
                }
                return true;
            }
        });

    }

    @Override
    synchronized public void onClick(View v) {
        /*
        switch (v.getId()) {
            case  R.id.reset_button: {
                initGfx();
                ((DrawField)findViewById(R.id.the_canvas)).knightMoveFlag = false;
                break;
            }
            case R.id.button_left: {
                ((DrawField) findViewById(R.id.the_canvas)).knightMoveFlag = true;
                ((DrawField) findViewById(R.id.the_canvas)).knightMoveRightFlag = false;
                ((DrawField) findViewById(R.id.the_canvas)).knightMoveLeftFlag = true;
                break;
            }
            case R.id.button_right: {
                ((DrawField)findViewById(R.id.the_canvas)).knightMoveFlag = true;
                ((DrawField)findViewById(R.id.the_canvas)).knightMoveLeftFlag = false;
                ((DrawField)findViewById(R.id.the_canvas)).knightMoveRightFlag = true;
                break;
            }
        }
        */
    }

    public void initKnights() {
        //logger.trace("---");

        k1 = new Knight("1", false, this);
        k2 = new Knight("2", true, this);



        //k1.x = 0;
        k1.x = 0 + k1.body.bodyRectangleWidth;
        //p2.x = 250;
        //k2.x = 100;
        k2.x = Const.IMAGE_RESIZE_VALUE * 100 + k2.body.bodyRectangleWidth;
        k2.recalcWeapon();
        k1.xSpeed = 0;
        k2.xSpeed = 0;
        k2.xBound = frameWidth - k2.objectWidth - 20;
    }

    public void drawImageSequenceTask() {
        //logger.trace("---");
        if(k1.drawAttackAttemptFlag) {
            ++k1.imageSequenceTaskCurrentIndex;
            if(k1.imageSequenceTaskCurrentIndex > 5) {
                k1.imageSequenceTaskCurrentIndex = 5;
            }
        }
    }

    public void enableCollisionFlags() {
        //logger.trace("---");
        k1.collisionFlag = true;
        k2.collisionFlag = true;
    }

    public void resetCollisionFlags() {
        //logger.trace("---");
        k1.collisionFlag = false;
        k2.collisionFlag = false;
    }

    public void calcMoving() {
        //logger.trace("---");
        k1.calcMoving(k2);
        k2.calcMoving(k1);
    }

    public void calcAttack() {
        //logger.trace("---");
        k2 = k1.calcAttack(k2, this);
        k1 = k2.calcAttack(k1, this);
    }

    public void calcHealthPointsBars() {
        //logger.trace("---");
        k1.calcHealthPointsBar(oneSecondPassed);
        k2.calcHealthPointsBar(oneSecondPassed);
    }

    public void calcStaminaBars() {
        //logger.trace("---");
        k1.calcStaminaBar(oneHalfSecondPassed);
        k2.calcStaminaBar(oneHalfSecondPassed);
    }

    public void recalcImages() {
        //logger.trace("---");
        k1.recalcImages();
        k2.recalcImages();
    }

    public void stepCadre() {
        //logger.trace("---");
        //logger.info("---");

        if(k1.playerDead || k2.playerDead) {
            return;
        }

        calcTimePassed();
        calcMoving();
        calcAttack();
        calcHealthPointsBars();
        calcStaminaBars();

        customPeriodTimePassed = false;
        oneHalfSecondPassed = false;
        oneSecondPassed = false;
        twoSecondsPassed = false;
        /*
        calcMoving();
        calcAttack();
        calcHealthPointsBars();
        calcStaminaBars();
        recalcImages();
        */

        ((DrawField)findViewById(R.id.the_canvas)).initKnights(k1, k2);
        //((TextView)findViewById(R.id.debug_text)).setText("123");
        //((TextView)findViewById(R.id.debug_text)).setText(k1.healthPointsBar.getDebugPrint());
        //((TextView)findViewById(R.id.debug_text)).setText(k1.body.getDebugPrint() + "\n" + k2.body.getDebugPrint());
    }

}
