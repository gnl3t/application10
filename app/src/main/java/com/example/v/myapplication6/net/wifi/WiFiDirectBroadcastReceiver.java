package com.example.v.myapplication6.net.wifi;


import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.wifi.p2p.*;

import com.example.v.myapplication6.Engine4;


public class WiFiDirectBroadcastReceiver extends BroadcastReceiver {

	private WifiP2pManager mManager;
	private WifiP2pManager.Channel mChannel;
	private Engine4 e;

	public boolean wifiP2pEnabledFlag;

	public WiFiDirectBroadcastReceiver(WifiP2pManager manager,
									   WifiP2pManager.Channel channel,
									   Engine4 e) {
		super();
		this.mManager = manager;
		this.mChannel = channel;
		this.e = e;
	}

	@Override
	public void onReceive(Context context, Intent intent) {
		String action = intent.getAction();

		if (WifiP2pManager.WIFI_P2P_STATE_CHANGED_ACTION.equals(action)) {
			// Check to see if Wi-Fi is enabled and notify appropriate activity
			int state = intent.getIntExtra(WifiP2pManager.EXTRA_WIFI_STATE, -1);
			if (state == WifiP2pManager.WIFI_P2P_STATE_ENABLED) {
				// Wifi P2P is enabled
				e.printDbgText("Wifi P2P is enabled");
				wifiP2pEnabledFlag = true;
			} else {
				// Wi-Fi P2P is not enabled
				e.printDbgText("Wi-Fi P2P is not enabled");
				wifiP2pEnabledFlag = false;
			}
		} else if (WifiP2pManager.WIFI_P2P_PEERS_CHANGED_ACTION.equals(action)) {
			// Call WifiP2pManager.requestPeers() to get a list of current peers
		} else if (WifiP2pManager.WIFI_P2P_CONNECTION_CHANGED_ACTION.equals(action)) {
			// Respond to new connection or disconnections
		} else if (WifiP2pManager.WIFI_P2P_THIS_DEVICE_CHANGED_ACTION.equals(action)) {
			// Respond to this device's wifi state changing
		}
	}
}
