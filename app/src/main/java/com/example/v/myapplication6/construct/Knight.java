package com.example.v.myapplication6.construct;


import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.View;

import com.example.v.myapplication6.R;
import com.example.v.myapplication6.construct.task.*;
import com.example.v.myapplication6.util.*;

import java.util.*;


public class Knight extends BasicObject {


	public int objectWidth = 50;
	public int objectHeight = 50;
	public double xSpeed;
	public double xBound;
	public double weaponSpeedForward;
	public double weaponSpeedBack;
	public boolean collisionFlag;
	public boolean moveForwardFlag;
	public boolean moveBackFlag;
	public boolean lowFlag;
	public boolean drawAttackAttemptFlag;
	public boolean drawAttackForwardAttemptFlag;
	public boolean drawAttackAttemptAllowFlag;
	public boolean initStageFlag = true;
	//public boolean attackMomentFinishedFlag;
	public boolean damageDone;
	public boolean staminaUsed;
	public boolean staminaForwardAttackUsed;
	public boolean drawShieldUp;
	public boolean drawShieldLow;
	public boolean playerDead;


	public Body body;
	public HealthPointsBar healthPointsBar;
	public StaminaBar staminaBar;
	public Weapon weapon;
	public Shield shield;


	public String imageId;
	public boolean revertedFlag;

	/*
	public Image image;
	public Image imageEmpty;
	public Image imageStand;
	public Image imageAttack;
	public Image imageShield;
	*/
	/*
	public Bitmap image;
	public Bitmap imageEmpty;
	public Bitmap imageStand;
	public Bitmap imageAttack;
	public Bitmap imageShield;
	*/
	public int intImageId;
	public int imageEmptyId;
	public int imageStandId;
	public int imageAttackId;
	public int imageAttackForwardId;
	public int imageAttackLowId;
	public int imageShieldId;
	public int imageShieldLowId;
	public int imageLowId;

	//public ImageTask imageTask = new ImageTask();
	public ImageTask imageTask;
	public ImageSequenceTask imageSequenceTask;
	public int imageSequenceTaskCurrentIndex;

	//may be replaced by big array 20-30 elements and being set to zero values in each cell on each refresh cycle
	//it avoids collection reinitialization and utilization but utilize many cell objects
	//maybe manual garbage collection call could be faster
	//or it can be used with arrays reinit
	//anyway good place for profiling
	public List<ImageTask> imageTasks = new ArrayList<ImageTask>();


	public void imagesProcessing() {
		//log.trace("---");

		intImageId = R.drawable.k1_002;
		imageEmptyId = R.drawable.k1_104_attack_empty;
		imageStandId = R.drawable.k1_003;
		imageAttackId = R.drawable.k1_004_attack;
		imageAttackForwardId = R.drawable.k1_007_attack_forward;
		imageAttackLowId = R.drawable.k1_009_attack_low;
		imageShieldId = R.drawable.k1_005_shield;
		imageShieldLowId = R.drawable.k1_008_shield_low;
		imageLowId = R.drawable.k1_010_low;




	  /*
	  images.put(R.drawable.k1_002, BitmapFactory.decodeResource(getResources(), R.drawable.k1_002));
	  images.put(R.drawable.knight1, BitmapFactory.decodeResource(getResources(), R.drawable.knight1));
	  images.put(R.drawable.k1_003, BitmapFactory.decodeResource(getResources(), R.drawable.k1_003));
	  images.put(R.drawable.k1_004_attack, BitmapFactory.decodeResource(getResources(), R.drawable.k1_004_attack));
	  images.put(R.drawable.k1_005_shield, BitmapFactory.decodeResource(getResources(), R.drawable.k1_005_shield));
	  images.put(R.drawable.k1_007_attack_forward, BitmapFactory.decodeResource(getResources(), R.drawable.k1_007_attack_forward));
		images.put(R.drawable.k1_008_shield_low, BitmapFactory.decodeResource(getResources(), R.drawable.k1_008_shield_low));
		images.put(R.drawable.k1_009_attack_low, BitmapFactory.decodeResource(getResources(), R.drawable.k1_009_attack_low));
		images.put(R.drawable.k1_010_low, BitmapFactory.decodeResource(getResources(), R.drawable.k1_010_low));
	  */

		//logger.info("Working Directory = " + System.getProperty("user.dir"));
	  /*
	try {
		//String pathString = "/img/knight1.png";
		//String pathString = "k" + imageId + "_0.02";
		//image = ImageIO.read(new File(Const.BASIC_PATH_IMG + pathString + ".png"));
		image = ImageIO.read(new File(Const.BASIC_PATH_IMG + "k" + "1" + "_1.04_attack1" + ".png"));
		imageEmpty = ImageIO.read(new File(Const.BASIC_PATH_IMG + "k" + "1" + "_1.04_attack_empty" + ".png"));
		if(!revertedFlag) {
			imageSequenceTask = new ImageSequenceTask(imageId, "_1.04_attack", 6);
		}

		imageStand = ImageIO.read(new File(Const.BASIC_PATH_IMG + "k" + imageId + "_0.03" + ".png"));
		imageAttack = ImageIO.read(new File(Const.BASIC_PATH_IMG + "k" + imageId + "_0.04_attack" + ".png"));
		imageAttackLow = ImageIO.read(new File(Const.BASIC_PATH_IMG + "k" + imageId + "_0.09_attack_low" + ".png"));
		imageAttackForward = ImageIO.read(new File(Const.BASIC_PATH_IMG + "k" + imageId + "_0.07_attack_forward" + ".png"));
		imageShield = ImageIO.read(new File(Const.BASIC_PATH_IMG + "k" + imageId + "_0.05_shield" + ".png"));
		imageShieldLow = ImageIO.read(new File(Const.BASIC_PATH_IMG + "k" + imageId + "_0.08_shield_low" + ".png"));
	} catch (Exception e1) {
		e1.printStackTrace();
	}
	*/
	}

	public void recalcImages() {
		//log.trace("---");
		if(imageTask.getImageId() == 0) {
			imageTask.setImageId(imageEmptyId);
		}

		imageTasks = new ArrayList<ImageTask>(); //refresh image tasks set
		imageTasks.add(imageTask); //legacy way support
		imageTask = new ImageTask(0, 0);
		if(!revertedFlag) {
			imageTask = imageSequenceTask.getImageTasks().get(imageSequenceTaskCurrentIndex);

			imageTasks.add(imageTask);

			imageTask = new ImageTask(0, 100);
			imageTasks.add(imageTask);
		}
	}

	//imageId = 1 or 2
	public Knight(String imageId, boolean revertedFlag, AbstractEngine e) {
		//log.trace("---");
		this.imageId = imageId;
		this.revertedFlag = revertedFlag;
		imagesProcessing();


		body = new Body(revertedFlag);
		healthPointsBar = new HealthPointsBar(e, revertedFlag);
		staminaBar = new StaminaBar(e, revertedFlag);
		weapon = new Weapon(revertedFlag,
				body.halfBodyRectangleWidthStartValue,
				body.halfBodyRectangleHeightStartValue);
		shield = new Shield(revertedFlag,
				body.halfBodyRectangleWidthStartValue,
				body.halfBodyRectangleHeightStartValue);

		weaponSpeedForward = Const.WEAPON_SPEED_FORWARD_VALUE;
		weaponSpeedBack = Const.WEAPON_SPEED_BACK_VALUE;

		initStageFlag = false;
	}

	public void recalcWeapon() {
		weapon.x = x;
	}

	public void calcMoving(Knight k2) {
		//log.trace("---");
		if(!collisionFlag && !drawAttackAttemptFlag) {
			x += xSpeed;
		}
		if(body.getBodyRectangle().intersect(k2.body.getBodyRectangle())
				|| body.getBodyRectangle().intersect(k2.weapon.getWeaponRectangle())) {
			collisionFlag = true;
		} else {
			collisionFlag = false;
		}
		//body.x = x;
		body.y = Const.SCENE_LEVEL;
		//body.width = body.x + body.bodyRectangleWidth;
		//body.height = body.y + body.bodyRectangleHeight;

		if(!revertedFlag) {
			body.x = x - body.halfBodyRectangleWidthStartValue;
			imageTask = new ImageTask((int) (x - body.bodyRectangleWidth), Const.SCENE_LEVEL);
		} else {
			body.x = x + body.bodyRectangleWidthStartValue + body.halfBodyRectangleWidthStartValue;
			imageTask = new ImageTask((int) x, Const.SCENE_LEVEL);
		}
		//imageTask.setImage(imageStand);
		//imageTask.setImageId(imageAttackId);

		if(lowFlag) {
			imageTask.setImageId(imageLowId);
		} else {
			imageTask.setImageId(imageStandId);
		}
	}

	public Knight dealDamageToOpponent(Knight k2, AbstractEngine e) {
		//log.trace("---");
		k2.drawAttackAttemptAllowFlag = true;
		if(!damageDone) {
			damageDone = true;
			k2.healthPointsBar.healthPoints = k2.healthPointsBar.healthPoints - weapon.weaponDamage;
			//logger.info("healthPoints2=" + healthPoints2);
			if(k2.healthPointsBar.healthPoints <= 0) {
				k2.playerDead = true;
				//logger.info("player2Dead");
				e.setWinnerMessage("player" + imageId + " win");
			}
		}

		return k2;
	}

	public Knight dealDamageOverShieldToOpponent(Knight k2, AbstractEngine e) {
		//log.trace("---");
		k2.drawAttackAttemptAllowFlag = true;
		if(!damageDone) {
			damageDone = true;
			k2.healthPointsBar.healthPoints = k2.healthPointsBar.healthPoints - 1;
			//logger.info("healthPoints2=" + healthPoints2);
			if(k2.healthPointsBar.healthPoints <= 0) {
				k2.playerDead = true;
				//logger.info("player2Dead");
				e.setWinnerMessage("player" + imageId + " win");
			}
		}

		return k2;
	}

	public Knight calcAttack(Knight k2, AbstractEngine e) {
		//log.trace("---");
		//damage calc
		///*
		if(drawAttackAttemptFlag && staminaBar.staminaPoints > 0) {
			if(weapon.getWeaponRectangle().intersect(k2.body.getBodyRectangle())) {
				//logger.info("dmg to 2!");
				if((!k2.drawShieldUp && !k2.drawShieldLow) ||
						(k2.drawShieldUp && lowFlag) ||
						(k2.drawShieldLow && !lowFlag)) {
					dealDamageToOpponent(k2, e);
				}
				else {
					//dealDamageOverShieldToOpponent(k2, e);
				}
			}
		}
		//*/

		calcWeaponCoordinates();
		calcShieldCoordinates();

		return k2;
	}

	public void calcWeaponCoordinates() {
		//log.trace("---");
		if(drawAttackAttemptFlag && staminaBar.staminaPoints > 0 && !drawShieldUp) {
			///*
			if(!revertedFlag) {
				if(moveForwardFlag) {
					//weapon.x = x + body.bodyRectangleWidthStartValue + 2 * weapon.weaponRectangleWidthStartValue + weapon.weaponRectangleAttackExtendWidth;
					weapon.x_final = //Const.IMAGE_RESIZE_VALUE *
							(x + body.bodyRectangleWidthStartValue + 2 * weapon.weaponRectangleWidthStartValue + weapon.weaponRectangleAttackExtendWidth);
					calcWeaponMoveForward();
				} else {
					//weapon.x = x + body.bodyRectangleWidthStartValue + weapon.weaponRectangleWidthStartValue + weapon.weaponRectangleAttackExtendWidth;
					weapon.x_final = //Const.IMAGE_RESIZE_VALUE *
							(x + body.bodyRectangleWidthStartValue + weapon.weaponRectangleWidthStartValue + weapon.weaponRectangleAttackExtendWidth);
					calcWeaponMoveForward();
				}
				imageTask = new ImageTask((int) (x - body.bodyRectangleWidth), Const.SCENE_LEVEL);
			} else {
				//weapon.x = x - weapon.weaponRectangleWidthStartValue;
				if(moveForwardFlag) {
					//weapon.x = x - 2 * weapon.weaponRectangleAttackExtendWidth - weapon.weaponRectangleAttackExtendWidth;
					weapon.x_final = //Const.IMAGE_RESIZE_VALUE *
							(x - 2 * weapon.weaponRectangleAttackExtendWidth - weapon.weaponRectangleAttackExtendWidth);
					calcWeaponMoveForward();
				} else {
					//weapon.x = x - weapon.weaponRectangleAttackExtendWidth - weapon.weaponRectangleAttackExtendWidth;
					weapon.x_final =  //Const.IMAGE_RESIZE_VALUE *
							(x - weapon.weaponRectangleAttackExtendWidth - weapon.weaponRectangleAttackExtendWidth);
					calcWeaponMoveForward();
				}

				imageTask = new ImageTask((int) (x - weapon.weaponRectangleAttackExtendWidth - weapon.weaponRectangleAttackExtendWidth), Const.SCENE_LEVEL);
				//imageTask = new ImageTask(x - body.bodyRectangleWidth, Const.SCENE_LEVEL);
			}
			if(lowFlag) {
				weapon.y = Const.SCENE_LEVEL + body.halfBodyRectangleHeightStartValue;
				imageTask.setImageId(imageAttackLowId);
			} else {
				weapon.y = Const.SCENE_LEVEL;
				if(moveForwardFlag) {
					imageTask.setImageId(imageAttackForwardId);
				} else {
					imageTask.setImageId(imageAttackId);
				}
			}
			weapon.width = weapon.weaponRectangleWidthStartValue + weapon.weaponRectangleAttackExtendWidth;
			weapon.height = weapon.weaponRectangleHeight;
			//imageTask.setImageId(imageAttackId);
			//*/
		} else {
			if(!revertedFlag) {
				//weapon.x = x + body.bodyRectangleWidthStartValue + weapon.weaponRectangleWidthStartValue;
				weapon.x_start = x + body.bodyRectangleWidthStartValue + weapon.weaponRectangleWidthStartValue;
				calcWeaponMoveBack();
			} else {
				//weapon.x = x;
				weapon.x_start = x;
				calcWeaponMoveBack();
			}
			if(lowFlag) {
				weapon.y = Const.SCENE_LEVEL + body.halfBodyRectangleHeightStartValue;
			} else {
				weapon.y = Const.SCENE_LEVEL;
			}
			weapon.width = weapon.weaponRectangleWidthStartValue;
			weapon.height = weapon.weaponRectangleHeight;
			//imageTask.setImageId(imageStandId);
		}
	}

	public void calcWeaponMoveForward() {
		if(!revertedFlag) {
			weapon.x += weaponSpeedForward;
			if(weapon.x > weapon.x_final) {
				weapon.x = weapon.x_final;
			}
		} else {
			weapon.x -= weaponSpeedForward;
			if(weapon.x < weapon.x_final) {
				weapon.x = weapon.x_final;
			}
		}
	}

	public void calcWeaponMoveBack() {
		if(!revertedFlag) {
			weapon.x -= weaponSpeedBack;
			if(weapon.x < weapon.x_start) {
				weapon.x = weapon.x_start;
			}
		} else {
			weapon.x += weaponSpeedBack;
			if(weapon.x > weapon.x_start) {
				weapon.x = weapon.x_start;
			}
		}
	}

	public void calcShieldCoordinates() {
		if(drawShieldUp || drawShieldLow) {
			if(!revertedFlag) {
				shield.x = x + shield.shieldRectangleWidthStartValue + weapon.weaponRectangleAttackExtendWidth;
				imageTask = new ImageTask((int) (x - body.bodyRectangleWidth), Const.SCENE_LEVEL);
			} else {
				shield.x = x - weapon.weaponRectangleAttackExtendWidth;
				imageTask = new ImageTask((int) x, Const.SCENE_LEVEL);
			}
			if(lowFlag) {
				shield.y = Const.SCENE_LEVEL + body.halfBodyRectangleHeightStartValue;
				imageTask.setImageId(imageShieldLowId);
				//imageTask.setImageId(imageShieldId);
			} else {
				shield.y = Const.SCENE_LEVEL;
				//imageTask.setImage(imageShield);
				imageTask.setImageId(imageShieldId);
			}
			shield.width = shield.shieldRectangleWidthStartValue;
			shield.height = shield.shieldRectangleHeight;

			//imageTask.setImage(imageShield);
		} else {
			if(!revertedFlag) {
				shield.x = x + body.bodyRectangleWidthStartValue + shield.shieldRectangleWidthStartValue;
			} else {
				shield.x = x;
			}
			shield.y = Const.SCENE_LEVEL + shield.shieldRectangleHeightStartValue;
			shield.width = shield.shieldRectangleWidthStartValue;
			shield.height = shield.shieldRectangleHeight;
		}
	}

	public void calcHealthPointsBar(boolean allowChangeFlag) {
		//log.trace("---");
		if(allowChangeFlag) {
			if(healthPointsBar.healthPoints < healthPointsBar.healthPointsStartValue) {
				healthPointsBar.healthPoints += healthPointsBar.healthPointsRegenSpeed;
			}
		}
	}

	public void calcStaminaBar(boolean allowChangeFlag) {
		//log.trace("---");
		if(allowChangeFlag && !drawAttackAttemptFlag) {
			if(staminaBar.staminaPoints < 0) {
				staminaBar.staminaPoints = 10;
			}
			if(staminaBar.staminaPoints < staminaBar.staminaPointsStartValue) {
				staminaBar.staminaPoints += staminaBar.staminaPointsRegenSpeed;
				if(staminaBar.staminaPoints > staminaBar.staminaPointsStartValue) {
					staminaBar.staminaPoints = staminaBar.staminaPointsStartValue;
				}
			}
		}
	}

	public void calcStaminaLoss(AbstractEngine e) {
		if(!drawAttackForwardAttemptFlag && !drawShieldUp && !staminaUsed) {
			staminaUsed = true;
			calcStaminaLossCommon(e);
		}
		if(drawAttackForwardAttemptFlag && !drawShieldUp && !staminaForwardAttackUsed) {
			staminaForwardAttackUsed = true;
			calcStaminaLossCommon(e);
		}
	}

	private void calcStaminaLossCommon(AbstractEngine e) {
		if(staminaBar.staminaPoints > 0) {
			staminaBar.staminaPoints = staminaBar.staminaPoints - staminaBar.staminaLoss;
		}
		e.resetEventsTimer();
	}

}
