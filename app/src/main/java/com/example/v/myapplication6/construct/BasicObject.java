package com.example.v.myapplication6.construct;


public class BasicObject  {
	public double x;
	public double y;
	public double width;
	public double height;

	public double x_start;
	public double y_start;
	public double width_start;
	public double height_start;

	public double x_final;
	public double y_final;
	public double width_final;
	public double height_final;

	  
  public void resetPrimitives() {
	  x = 0;
	  y = 0;
	  width = 0;
	  height = 0;
  }

	public String getDebugPrint() {
		String debugPrint = "";
		debugPrint += "\n" + "x=" + x;
		debugPrint += "\n" + "y=" + y;
		debugPrint += "\n" + "width=" + width;
		debugPrint += "\n" + "height=" + height;
		return debugPrint;
	}
  
}
