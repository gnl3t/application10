package com.example.v.myapplication6.util;


public class Const {
    //Divide the frame by 1000 to calculate how many times per second the screen will update.
    public static final int FRAME_RATE = 20; //50 frames per second

    public static final int IMAGE_RESIZE_VALUE = 5;
    public static final int ANDROID_RESIZE_VALUE = 2;


    public static final int SCENE_LEVEL = 100;
    public static final int HEALTH_POINTS_BAR_LEVEL = 20;
    public static final int STAMINA_BAR_LEVEL = 34;
    public static final int HEALTH_POINTS_BAR_HEIGHT = 12;
    public static final int STAMINA_BAR_HEIGHT = 7;
    public static final int X_SPEED_VALUE = Const.IMAGE_RESIZE_VALUE * 1;

    public static final double WEAPON_SPEED_FORWARD_VALUE = Const.IMAGE_RESIZE_VALUE * 0.3;
    public static final double WEAPON_SPEED_FORWARD_ACCELERATE_VALUE = Const.IMAGE_RESIZE_VALUE * 0.3;
    public static final double WEAPON_SPEED_BACK_VALUE = Const.IMAGE_RESIZE_VALUE * 0.3;
    public static final double WEAPON_SPEED_BACK_ACCELERATE_VALUE = Const.IMAGE_RESIZE_VALUE * 0.3;



    /*
    public static final String BASIC_PATH = "C:/___files/___code/___eclipse/app1";
    public static final String BASIC_PATH_IMG = BASIC_PATH + "/img/";
    */

}
