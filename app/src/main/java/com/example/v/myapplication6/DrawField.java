package com.example.v.myapplication6;


import android.content.Context;
import android.graphics.*;
import android.util.AttributeSet;
import android.view.View;

import com.example.v.myapplication6.construct.*;
import com.example.v.myapplication6.construct.task.*;
import com.example.v.myapplication6.util.Const;

import java.util.*;


public class DrawField extends View {

	Paint paint = new Paint();
	public boolean knightMoveFlag = false;
	public boolean knightMoveLeftFlag = false;
	public boolean knightMoveRightFlag = false;
	private int x = 0;
	private int xSpeed = 1;
	private Bitmap bmp1 = null;
	private Bitmap bmp2 = null;
	private Bitmap bmp3 = null;
	private Bitmap bmp = null;
	private Bitmap tempBmp = null;
	public Map<Integer, Bitmap> images = new HashMap<Integer, Bitmap>();

	Knight k1;
	Knight k2;

	private boolean allowFields = true;
	private boolean allowImages = true;
	private boolean allowDrawFieldDbg = false;

	public long imagesInitCounter = 0;


	public DrawField(Context context, AttributeSet aSet) {
		super(context, aSet);
		//bmp1 = BitmapFactory.decodeResource(getResources(), R.mipmap.ic_launcher_round);
		bmp1 = BitmapFactory.decodeResource(getResources(), R.drawable.knight1);
		bmp2 = BitmapFactory.decodeResource(getResources(), R.drawable.knight2);
		bmp3 = BitmapFactory.decodeResource(getResources(), R.drawable.k1_002);
		bmp = BitmapFactory.decodeResource(getResources(), R.drawable.knight1);



		images.put(R.drawable.k1_002, BitmapFactory.decodeResource(getResources(), R.drawable.k1_002));
		images.put(R.drawable.knight1, BitmapFactory.decodeResource(getResources(), R.drawable.knight1));
		images.put(R.drawable.k1_003, BitmapFactory.decodeResource(getResources(), R.drawable.k1_003));
		images.put(R.drawable.k1_004_attack, BitmapFactory.decodeResource(getResources(), R.drawable.k1_004_attack));
		images.put(R.drawable.k1_005_shield, BitmapFactory.decodeResource(getResources(), R.drawable.k1_005_shield));
		images.put(R.drawable.k1_007_attack_forward, BitmapFactory.decodeResource(getResources(), R.drawable.k1_007_attack_forward));
		images.put(R.drawable.k1_008_shield_low, BitmapFactory.decodeResource(getResources(), R.drawable.k1_008_shield_low));
		images.put(R.drawable.k1_009_attack_low, BitmapFactory.decodeResource(getResources(), R.drawable.k1_009_attack_low));
		images.put(R.drawable.k1_010_low, BitmapFactory.decodeResource(getResources(), R.drawable.k1_010_low));

		resizeImages();
		++imagesInitCounter;

		 /*
	try {
		//String pathString = "/img/knight1.png";
		//String pathString = "k" + imageId + "_0.02";
		//image = ImageIO.read(new File(Const.BASIC_PATH_IMG + pathString + ".png"));
		image = ImageIO.read(new File(Const.BASIC_PATH_IMG + "k" + "1" + "_1.04_attack1" + ".png"));
		imageEmpty = ImageIO.read(new File(Const.BASIC_PATH_IMG + "k" + "1" + "_1.04_attack_empty" + ".png"));
		if(!revertedFlag) {
			imageSequenceTask = new ImageSequenceTask(imageId, "_1.04_attack", 6);
		}

		imageStand = ImageIO.read(new File(Const.BASIC_PATH_IMG + "k" + imageId + "_0.03" + ".png"));
		imageAttack = ImageIO.read(new File(Const.BASIC_PATH_IMG + "k" + imageId + "_0.04_attack" + ".png"));
		imageShield = ImageIO.read(new File(Const.BASIC_PATH_IMG + "k" + imageId + "_0.05_shield" + ".png"));
	} catch (Exception e1) {
		e1.printStackTrace();
	}
	*/
	}

	public void resizeImages() {
		Map<Integer, Bitmap> resizedImages = new HashMap<Integer, Bitmap>();

		for (Map.Entry<Integer, Bitmap> entry : images.entrySet())
		{
			tempBmp = entry.getValue();
			System.out.println(entry.getKey() + "/" + entry.getValue());
			resizedImages.put(entry.getKey(), Bitmap.createScaledBitmap(tempBmp,
					Const.IMAGE_RESIZE_VALUE * tempBmp.getWidth(),
					Const.IMAGE_RESIZE_VALUE * tempBmp.getHeight(),
					true));
		}
		images = resizedImages;
	}
	
	@Override
	synchronized public void onDraw(Canvas canvas) {
		//canvas.drawColor(Color.WHITE);
		//canvas.drawColor(Color.TRANSPARENT, PorterDuff.Mode.CLEAR);

		if(allowDrawFieldDbg) {
			if (knightMoveFlag) {
				if (knightMoveLeftFlag) {
					bmp = bmp2;
					xSpeed = -1;
					if (x == getWidth() - bmp.getWidth()) {
						xSpeed = -1;
					}
					if (x == 0) {
						xSpeed = 1;
					}
				} else if (knightMoveRightFlag) {
					bmp = bmp1;
					xSpeed = 1;
					if (x == getWidth() - bmp.getWidth()) {
						xSpeed = -1;
					}
					if (x == 0) {
						xSpeed = 1;
					}
				}
				x = x + xSpeed;
			}
			canvas.drawBitmap(bmp, x, 10, null);

			paint.setColor(Color.BLACK);
			paint.setStrokeWidth(3);
			paint.setStyle(Paint.Style.STROKE);
			canvas.drawRect(
					x,//left
					200,//top
					x + 80,//right
					280,//bottom
					paint);
		}

		render(canvas);
	}

	public void initKnights(Knight k1, Knight k2) {
		//logger.trace("---");


		this.k1 = k1;
		this.k2 = k2;
	}



	public void render(Canvas canvas) {
		//logger.trace("---");
		if(k1 == null || k2 == null) {
			return;
		}
		if(allowDrawFieldDbg) {
			paint.setColor(Color.BLACK);
			paint.setStrokeWidth(3);
			paint.setStyle(Paint.Style.STROKE);
			canvas.drawRect(
					x,//left
					300,//top
					x + 80,//right
					380,//bottom
					paint);
		}

		paint.setColor(Color.RED);
		paint.setStrokeWidth(1);
		paint.setStyle(Paint.Style.STROKE);
		canvas.drawRect(k1.healthPointsBar.getHealthPointsBarBounds(), paint);
		canvas.drawRect(k2.healthPointsBar.getHealthPointsBarBounds(), paint);
		paint.setStyle(Paint.Style.FILL);
		canvas.drawRect(k1.healthPointsBar.getHealthPointsBar(), paint);
		canvas.drawRect(k2.healthPointsBar.getHealthPointsBar(), paint);
		paint.setColor(Color.GRAY);
		paint.setStyle(Paint.Style.STROKE);
		canvas.drawRect(k1.staminaBar.getStaminaBarBounds(), paint);
		canvas.drawRect(k2.staminaBar.getStaminaBarBounds(), paint);
		paint.setStyle(Paint.Style.FILL);
		canvas.drawRect(k1.staminaBar.getStaminaBar(), paint);
		canvas.drawRect(k2.staminaBar.getStaminaBar(), paint);

		paint.setColor(Color.RED);
		paint.setStyle(Paint.Style.FILL);
		if (k1.playerDead) {
			canvas.drawRect(k1.body.getBodyRectangle(), paint);
		}
		if (k2.playerDead) {
			canvas.drawRect(k2.body.getBodyRectangle(), paint);
		}

		if (allowFields) {
			paint.setColor(Color.GRAY);
			paint.setStyle(Paint.Style.FILL);
			canvas.drawRect(k1.shield.getShieldRectangle(), paint);
			canvas.drawRect(k2.shield.getShieldRectangle(), paint);
			paint.setColor(Color.GREEN);
			paint.setStyle(Paint.Style.STROKE);
			canvas.drawRect(k1.body.getBodyRectangle(), paint);
			//paint.setColor(Color.BLUE);
			canvas.drawRect(k2.body.getBodyRectangle(), paint);
			paint.setColor(Color.RED);
			canvas.drawRect(k1.weapon.getWeaponRectangle(), paint);
			canvas.drawRect(k2.weapon.getWeaponRectangle(), paint);
		}

		if (allowImages) {
			///*
			k1.imageTasks.clear();
			k1.imageTasks.add(k1.imageTask);
			k2.imageTasks.clear();
			k2.imageTasks.add(k2.imageTask);
			//*/

			int counter = 0;
			for (ImageTask imageTask : k1.imageTasks) {
				++counter;
				if (counter > 0) {
					//g2.drawImage(imageTask.getImage(), imageTask.getX(), imageTask.getY(), this);
					//canvas.drawBitmap(bmp3, imageTask.getX(), imageTask.getY(), null);

					//canvas.drawBitmap(images.get(imageTask.getImageId()), imageTask.getX(), imageTask.getY(), null);
					canvas.drawBitmap(images.get(imageTask.getImageId()),
							imageTask.getX() -  3 * (int)k1.body.halfBodyRectangleWidthStartValue / Const.IMAGE_RESIZE_VALUE,
							imageTask.getY() -  2 * (int)k1.body.halfBodyRectangleHeightStartValue / Const.IMAGE_RESIZE_VALUE,
							null);
				}
			}

			for (ImageTask imageTask : k2.imageTasks) {
				//g2.drawImage(imageTask.getImage(), imageTask.getX(), imageTask.getY(), this);
			}
		}
	}

}
