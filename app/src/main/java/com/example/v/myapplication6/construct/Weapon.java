package com.example.v.myapplication6.construct;


import android.graphics.Rect;

import com.example.v.myapplication6.util.Const;

public class Weapon extends BasicObject  {
	
	private Rect weaponRectangle = new Rect();

	public double weaponRectangleWidthStartValue;
	public double weaponRectangleWidth;

	public double weaponRectangleHeightStartValue;
	public double weaponRectangleHeight;

	public double weaponRectangleAttackExtendWidth = Const.IMAGE_RESIZE_VALUE * Const.ANDROID_RESIZE_VALUE *  5;
	public double weaponDamageStartValue = 10;
	public double weaponDamage = weaponDamageStartValue;


	private boolean revertedFlag;
	  
	
	public Weapon(boolean revertedFlag,
				  double halfBodyRectangleWidthStartValue,
				  double halfBodyRectangleHeightStartValue) {
		//logger.trace("---");
		this.revertedFlag = revertedFlag;
		
		weaponRectangleWidthStartValue = halfBodyRectangleWidthStartValue;
		weaponRectangleWidth = halfBodyRectangleWidthStartValue;
		
		weaponRectangleHeightStartValue = halfBodyRectangleHeightStartValue;
		weaponRectangleHeight = halfBodyRectangleHeightStartValue;

		this.width = weaponRectangleWidth;
		this.height = weaponRectangleHeight;
	}

	/*
	public void init() {
		logger.trace("---");
		  p1.collisionFlag = true;
		  p2.collisionFlag = true;
	  }
	*/

	public void extendedDebugPrint() {
		/*
		logger.info("getDebugPrint()=" + getDebugPrint());
		logger.info("\n" + "revertedFlag=" + revertedFlag);
		*/
	}

	public Rect getWeaponRectangle() {
		//weaponRectangle.set(x, y, x + width, y + height);
		weaponRectangle.set((int) x, (int) y,(int) (x + width), (int) (y + height));
		return weaponRectangle;
	}

	public void setWeaponRectangle(Rect weaponRectangle) {
		this.weaponRectangle = weaponRectangle;
	}
	
	
	
}
