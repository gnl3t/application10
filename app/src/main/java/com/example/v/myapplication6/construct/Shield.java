package com.example.v.myapplication6.construct;


import android.graphics.Rect;

public class Shield extends BasicObject  {

	private Rect shieldRectangle = new Rect();

	public double shieldRectangleWidthStartValue;
	public double shieldRectangleWidth;

	public double shieldRectangleHeightStartValue;
	public double shieldRectangleHeight;

	private boolean revertedFlag;
	  
	
	public Shield(boolean revertedFlag,
				  double halfBodyRectangleWidthStartValue,
				  double halfBodyRectangleHeightStartValue) {
		//logger.trace("---");
		this.revertedFlag = revertedFlag;

		shieldRectangleWidthStartValue = halfBodyRectangleWidthStartValue;
		shieldRectangleWidth = halfBodyRectangleWidthStartValue;
		
		shieldRectangleHeightStartValue = halfBodyRectangleHeightStartValue;
		shieldRectangleHeight = halfBodyRectangleHeightStartValue;

		this.width = shieldRectangleWidth;
		this.height = shieldRectangleHeight;
	}

	/*
	public void init() {
		logger.trace("---");
		  p1.collisionFlag = true;
		  p2.collisionFlag = true;
	  }
	*/

	public void extendedDebugPrint() {
		/*
		logger.info("getDebugPrint()=" + getDebugPrint());
		logger.info("\n" + "revertedFlag=" + revertedFlag);
		*/
	}

	public Rect getShieldRectangle() {
		//shieldRectangle.set(x, y, x + width, y + height);
		shieldRectangle.set((int) x, (int) y,(int) (x + width), (int) (y + height));
		return shieldRectangle;
	}

	public void setShieldRectangle(Rect shieldRectangle) {
		this.shieldRectangle = shieldRectangle;
	}
	
}
