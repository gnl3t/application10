package com.example.v.myapplication6.construct.task;




public class ImageTask {
	//private Image image;
	private int imageId = 0;
	private int x = 0;
	private int y = 0;
	
	public ImageTask() {
		
	}
	
	public ImageTask(int x, int y) {
		this.setX(x);
		this.setY(y);
	}


	public int getImageId() {
		return imageId;
	}

	public void setImageId(int imageId) {
		this.imageId = imageId;
	}

	public int getX() {
		return x;
	}

	public void setX(int x) {
		this.x = x;
	}

	public int getY() {
		return y;
	}

	public void setY(int y) {
		this.y = y;
	}

	
}
