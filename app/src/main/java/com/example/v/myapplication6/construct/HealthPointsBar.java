package com.example.v.myapplication6.construct;


import android.graphics.Rect;

import com.example.v.myapplication6.util.Const;

public class HealthPointsBar extends BasicObject  {
	
	private Rect healthPointsBar = new Rect();
	private Rect healthPointsBarBounds = new Rect();

	public double healthPointsStartValue = Const.ANDROID_RESIZE_VALUE * 144;
	public double healthPoints = healthPointsStartValue;
	public double healthPointsRegenSpeed = 1;

	private AbstractEngine e;
	private boolean revertedFlag;
	  
	
	@SuppressWarnings("static-access")
	public HealthPointsBar(AbstractEngine e, boolean revertedFlag) {
		this.e = e;
		this.revertedFlag = revertedFlag;
		if(!revertedFlag){
			x = 3;
			y = Const.HEALTH_POINTS_BAR_LEVEL;
			width = healthPoints;
			height = Const.HEALTH_POINTS_BAR_HEIGHT;
		} else {
			x = e.frameWidth - healthPoints - 20;
			y = Const.HEALTH_POINTS_BAR_LEVEL;
			width = healthPoints;
			height = Const.HEALTH_POINTS_BAR_HEIGHT;
		}
		//healthPointsBarBounds.set(x, y, x + width, y + height);
		healthPointsBarBounds.set((int) x, (int) y,(int) (x + width), (int) (y + height));
	}

	public String extendedDebugPrint() {
		/*
		logger.info("getDebugPrint()=" + getDebugPrint());
		logger.info("\n" + "revertedFlag=" + revertedFlag);
		logger.info("\n" + "healthPoints=" + healthPoints);
		*/
		String result = "";
		result += "x=" + x;

		return result;
	}


	@SuppressWarnings("static-access")
	public Rect getHealthPointsBar() {
		if(!revertedFlag){
			x = 3;
			y = Const.HEALTH_POINTS_BAR_LEVEL;
			width = healthPoints;
			height = Const.HEALTH_POINTS_BAR_HEIGHT;
		} else {
			x = e.frameWidth - healthPoints - 20;
			y = Const.HEALTH_POINTS_BAR_LEVEL;
			width = healthPoints;
			height = Const.HEALTH_POINTS_BAR_HEIGHT;
		}
		//healthPointsBar.set(x, y, x + width, y + height);
		healthPointsBar.set((int) x, (int) y,(int) (x + width), (int) (y + height));
		return healthPointsBar;
	}
	
	public void setHealthPointsBar(Rect healthPointsBar) {
		this.healthPointsBar = healthPointsBar;
	}

	public Rect getHealthPointsBarBounds() {
		return healthPointsBarBounds;
	}

	public void setHealthPointsBarBounds(Rect healthPointsBarBounds) {
		this.healthPointsBarBounds = healthPointsBarBounds;
	}
	
}
