package com.example.v.myapplication6.construct;


import android.graphics.Rect;

import com.example.v.myapplication6.util.Const;


public class Body extends BasicObject  {
	//final static Logger logger = Logger.getLogger(Body.class);

	Rect bodyRectangle = new Rect();
	//private Rectangle2D.Double bodyRectangle = new Rectangle2D.Double();


	//public int bodyRectangleWidthStartValue = 50;
	public double bodyRectangleWidthStartValue = 16 * Const.IMAGE_RESIZE_VALUE;
	public double bodyRectangleWidth = bodyRectangleWidthStartValue;

	public double bodyRectangleHeightStartValue = 50 * Const.IMAGE_RESIZE_VALUE;
	public double bodyRectangleHeight = bodyRectangleHeightStartValue;

	public double halfBodyRectangleWidthStartValue = bodyRectangleWidthStartValue / 2;
	public double halfBodyRectangleHeightStartValue = bodyRectangleHeightStartValue / 2;

	private boolean revertedFlag;
	  
	
	public Body(boolean revertedFlag) {
		//logger.trace("---");
		this.revertedFlag = revertedFlag;

		this.width = bodyRectangleWidth;
		this.height = bodyRectangleHeight;
	}

	/*
	public void init() {
		logger.trace("---");
		  p1.collisionFlag = true;
		  p2.collisionFlag = true;
	  }
	*/

	public void extendedDebugPrint() {
		//logger.info("getDebugPrint()=" + getDebugPrint());
		//logger.info("\n" + "revertedFlag=" + revertedFlag);
	}

	public Rect getBodyRectangle() {
		//bodyRectangle.setFrame(x, y, width, height);
		bodyRectangle.set((int) x, (int) y,(int) (x + width), (int) (y + height));
		return bodyRectangle;
	}

	public void setBodyRectangle(Rect bodyRectangle) {
		this.bodyRectangle = bodyRectangle;
	}
	
}
