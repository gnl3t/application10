package com.example.v.myapplication6.construct;


import android.graphics.Rect;

import com.example.v.myapplication6.util.Const;

public class StaminaBar extends BasicObject  {
	
	private Rect staminaBar = new Rect();
	private Rect staminaBarBounds = new Rect();

	public int staminaPointsStartValue = Const.ANDROID_RESIZE_VALUE * 144;
	public int staminaPoints = staminaPointsStartValue;
	public int staminaPointsRegenSpeed = 20;
	
	public int staminaLossStartValue = 20;
	public int staminaLoss = staminaLossStartValue;

	private AbstractEngine e;
	private boolean revertedFlag;
	  
	
	@SuppressWarnings("static-access")
	public StaminaBar(AbstractEngine e, boolean revertedFlag) {
		//logger.trace("---");
		this.e = e;
		this.revertedFlag = revertedFlag;
		if(!revertedFlag){
			x = 3;
			y = Const.STAMINA_BAR_LEVEL;
			width = staminaPoints;
			height = Const.STAMINA_BAR_HEIGHT;
		} else {
			x = e.frameWidth - staminaPoints - 20;
			y = Const.STAMINA_BAR_LEVEL;
			width = staminaPoints;
			height = Const.STAMINA_BAR_HEIGHT;
		}
		//staminaBarBounds.set(x, y, x + width, y + height);
		staminaBarBounds.set((int) x, (int) y,(int) (x + width), (int) (y + height));
	}

	public void extendedDebugPrint() {
		/*
		logger.info("getDebugPrint()=" + getDebugPrint());
		logger.info("\n" + "revertedFlag=" + revertedFlag);
		logger.info("\n" + "staminaPoints=" + staminaPoints);
		*/
	}

	@SuppressWarnings("static-access")
	public Rect getStaminaBar() {
		if(!revertedFlag){
			x = 3;
			y = Const.STAMINA_BAR_LEVEL;
			width = staminaPoints;
			height = Const.STAMINA_BAR_HEIGHT;
		} else {
			x = e.frameWidth - staminaPoints - 20;
			y = Const.STAMINA_BAR_LEVEL;
			width = staminaPoints;
			height = Const.STAMINA_BAR_HEIGHT;
		}
		//staminaBar.set(x, y, x + width, y + height);
		staminaBar.set((int) x, (int) y,(int) (x + width), (int) (y + height));
		return staminaBar;
	}
	
	public void setStaminaBar(Rect staminaBar) {
		this.staminaBar = staminaBar;
	}

	public Rect getStaminaBarBounds() {
		return staminaBarBounds;
	}

	public void setStaminaBarBounds(Rect staminaBarBounds) {
		this.staminaBarBounds = staminaBarBounds;
	}
	
}
